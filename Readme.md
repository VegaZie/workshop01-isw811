# Workshop01 - Bookworm con Vagrant

## Instalación de VirtualBox

Se procede a descargar VirualBox desde el siguiente enlace

- [Descargar VirtualBox](https://www.virtualbox.org/wiki/Downloads)

## Insatalación de Vagrant

Se procede a descargar Vagrant desde el siguiente enlace:

- [Descargar Vagrant](https://www.vagrantup.com/downloads)

## Aprovisionar máquina Debian

Para aprovisionar una máquina Debian necesitaremos crear la estructura de directorios que almacenará las definiciones de las máquinas virtuales, de la siguiente manera.

```bash
cd ~
mkdir -p ISW811/VMs/webserver
cd ISW811/VMs/webserver
```

Ahora creamos el VagrantFile

```bash
vagrant init debian/bookworm64
```

Luego editamos el Vagrantfile. Para habilitar la red privada con la IP 192.168.37.10 a editar y descomentar la linea 375, dejandola de la siguiente manera

```bash
  config.vm.network "private_network", ip: "192.168.37.10"
```

Antes de lanzar el aprovisionamiento de la máquina virtual debemos asegurarnos de que el nivel de VirtualBOx exista la definición de la `vboxnet` de la red privada que se desautilizar con la máquina Vagrant

![Configuración de vboxnet](images/red.PNG)

A continuación algunos comandos para iniciar Vagrant desde bash:

```bash
  vagrant up   #para encender la máquina
  vagrant ssh  #para entrar en la máquina
  exit         #para salir de la máquina
  vagrant halt #para apagar la máquina
```